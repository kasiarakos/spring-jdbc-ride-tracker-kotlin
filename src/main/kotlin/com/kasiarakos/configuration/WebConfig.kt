package com.kasiarakos.configuration

import com.kasiarakos.repositories.mappers.RideRowMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import javax.sql.DataSource


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = ["com.kasiarakos"])
open class WebConfig {

    @Bean
    open fun mysqlDataSource(): DataSource {
        val dataSource = DriverManagerDataSource()
        dataSource.setDriverClassName("com.mysql.jdbc.Driver")
        dataSource.url ="jdbc:mysql://localhost:3306/ride_tracker"
        dataSource.username = "root"
        dataSource.password = "password"
        return dataSource
    }

    @Bean
    open fun jdbcTemplate(dataSource: DataSource): JdbcTemplate {
        val jdbcTemplate = JdbcTemplate(dataSource)
        jdbcTemplate.isResultsMapCaseInsensitive = true
        return jdbcTemplate
    }
}