package com.kasiarakos.configuration

import org.springframework.web.WebApplicationInitializer
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext
import org.springframework.web.servlet.DispatcherServlet
import javax.servlet.ServletContext

open class WebAppInitializer : WebApplicationInitializer {

    override fun onStartup(servletContext: ServletContext) {
        val ctx = AnnotationConfigWebApplicationContext()
        ctx.register(WebConfig::class.java)
        ctx.servletContext = servletContext

        val servlet = servletContext.addServlet("dispatcher", DispatcherServlet(ctx))
        servlet.setLoadOnStartup(1)
        servlet.addMapping("/*")
    }
}