package com.kasiarakos.model

data class Ride(var id: Int?, var name: String , var duration: Int)