package com.kasiarakos.controllers

import com.kasiarakos.model.Ride
import com.kasiarakos.services.RideService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/rides")
class RideController(private val rideService: RideService) {

    @GetMapping
    fun getRides() = rideService.getRides()

    @PostMapping
    fun createRide(@RequestBody ride: Ride) = rideService.create(ride)

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Int) = rideService.getById(id)

    @PutMapping
    fun update(@RequestBody ride: Ride) = rideService.update(ride)

    @PutMapping("/batch")
    fun batch()  = rideService.batch()

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Int) = rideService.deleteById(id)

}