package com.kasiarakos.services.impl

import com.kasiarakos.model.Ride
import com.kasiarakos.repositories.RideRepo
import com.kasiarakos.services.RideService
import org.springframework.stereotype.Service
import java.util.*

@Service
class RideServiceImpl(private val rideRepo: RideRepo) : RideService {
    override fun getRides() = rideRepo.getRides()
    override fun create(ride: Ride): Ride = rideRepo.create(ride)
    override fun getById(id: Int): Ride = rideRepo.getById(id)
    override fun update(ride: Ride): Ride = rideRepo.update(ride)
    override fun batch() {
        val rides = rideRepo.getRides().map {  arrayOf<Any?>(Date(), it.id) }
        rideRepo.updateRides(rides)
    }

    override fun deleteById(id: Int) = rideRepo.deleteById(id)

}