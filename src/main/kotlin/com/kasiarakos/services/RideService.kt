package com.kasiarakos.services

import com.kasiarakos.model.Ride

interface RideService {
    fun getRides() : List<Ride>
    fun create(ride: Ride): Ride
    fun getById(id: Int) : Ride
    fun update(ride: Ride): Ride
    fun batch()
    fun deleteById(id: Int): Any
}