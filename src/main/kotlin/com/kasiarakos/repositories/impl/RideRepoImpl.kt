package com.kasiarakos.repositories.impl

import com.kasiarakos.model.Ride
import com.kasiarakos.repositories.RideRepo
import com.kasiarakos.repositories.RideRepo.RideRepoObj.INSERT_MESSAGE_SQL
import com.kasiarakos.repositories.mappers.RideRowMapper
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.jdbc.support.KeyHolder
import org.springframework.stereotype.Repository
import java.sql.Statement


@Repository
class RideRepoImpl(
        private val jdbcTemplate: JdbcTemplate,
        private val rideRowMapper: RideRowMapper) : RideRepo{


    override fun getRides(): List<Ride> {
        val rides = jdbcTemplate
        return jdbcTemplate.query("select * from RIDES", rideRowMapper)
    }

    override fun create(ride: Ride): Ride {
        val keyHolder: KeyHolder = GeneratedKeyHolder()
        jdbcTemplate.update ({
           it.prepareStatement(INSERT_MESSAGE_SQL, Statement.RETURN_GENERATED_KEYS).also { prepStatement ->
                prepStatement.setString(1, ride.name)
                prepStatement.setInt(2, ride.duration)
            }

        }, keyHolder)

        return getById(keyHolder.key.toInt())
    }

    override fun getById(id: Int): Ride {
        return jdbcTemplate.queryForObject("select * from RIDES where id = ?", rideRowMapper, id)
    }

    override fun update(ride: Ride): Ride {
        jdbcTemplate.update("update RIDES set name =?, duration =? where id = ?", ride.name, ride.duration, ride.id)
        return getById(ride.id!!)
    }

    override fun updateRides(pairs: List<Array<Any?>>) {
        jdbcTemplate.batchUpdate("UPDATE RIDES set RIDE_DATE = ? where ID = ?", pairs)
    }

    override fun deleteById(id: Int) {
        jdbcTemplate.update("DELETE FROM RIDES WHERE ID = ?", id)
    }

}