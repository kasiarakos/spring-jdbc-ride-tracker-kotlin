package com.kasiarakos.repositories.mappers

import com.kasiarakos.model.Ride
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class RideRowMapper : RowMapper<Ride> {
    override fun mapRow(rs: ResultSet, p1: Int): Ride? {
        return Ride(rs.getInt("ID"),
                rs.getString("Name"),
                rs.getInt("DURATION"))
    }
}