package com.kasiarakos.repositories

import com.kasiarakos.model.Ride

interface RideRepo {

    companion object RideRepoObj {
        const val  INSERT_MESSAGE_SQL: String = "INSERT INTO RIDES (NAME, DURATION) VALUES(?, ?);"
    }
    fun getRides() : List<Ride>
    fun create(ride: Ride) : Ride
    fun getById(id: Int) : Ride
    fun update(ride: Ride): Ride
    fun updateRides(rides: List<Array<Any?>>)
    fun deleteById(id: Int)
}