package com.kasiarakos.controllers

import com.kasiarakos.model.Ride
import org.junit.Test
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

class RestControllerTest {

    @Test(timeout = 3000)
    fun testGetRides() {
        val restTemplate = RestTemplate()
        val ridesResponse: ResponseEntity<List<Ride>> = restTemplate.exchange<List<Ride>>(
                "http://localhost:8080/rides", HttpMethod.GET,
                null, object : ParameterizedTypeReference<List<Ride?>?>() {})
        val rides = ridesResponse.body
        for ((name) in rides) {
            println("Ride name: $name")
        }
    }

    @Test(timeout = 3000)
    fun testCreateRides() {
        val restTemplate = RestTemplate()
        val ride = Ride(1, "kastoria ride", 35)
        val result = restTemplate.postForEntity("http://localhost:8080/rides", ride, Ride::class.java)
    }

    @Test(timeout = 3000)
    fun testGetRideById() {
        val restTemplate = RestTemplate()
        val response = restTemplate.getForEntity("http://localhost:8080/rides/1", Ride::class.java)
        println(response.body)
    }

    @Test(timeout = 3000)
    fun testUpdateRide() {

        val restTemplate = RestTemplate()
        val response = restTemplate.getForEntity("http://localhost:8080/rides/1", Ride::class.java)
        val ride = response.body

        restTemplate.put("http://localhost:8080/rides", Ride(ride.id, ride.name, ride.duration + 1))
        val newResponse = restTemplate.getForEntity("http://localhost:8080/rides/1", Ride::class.java)
        println(newResponse)
    }


    @Test(timeout = 3000)
    fun testBatchUpdate() {
        val restTemplate = RestTemplate()
         restTemplate.put("http://localhost:8080/rides/batch", Object::class.java)
    }


    @Test(timeout = 3000)
    fun testDelete() {
        val restTemplate = RestTemplate()
        val response = restTemplate.delete("http://localhost:8080/rides/16")
    }
}